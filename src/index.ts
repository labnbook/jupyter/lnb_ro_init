import {
  JupyterFrontEnd,
  JupyterFrontEndPlugin
} from '@jupyterlab/application';

import { ISettingRegistry } from '@jupyterlab/settingregistry';

import { NotebookModel } from '@jupyterlab/notebook';

import { INotebookTracker } from "@jupyterlab/notebook";

import { IThemeManager } from '@jupyterlab/apputils';

import { Cell, ICellModel } from '@jupyterlab/cells';

import { PanelLayout } from '@lumino/widgets';

/**
 * Initialization data for the lnb_ro_init extension.
 */
const plugin: JupyterFrontEndPlugin<void> = {
  id: 'lnb_ro_init:plugin',
  autoStart: true,
  optional: [ISettingRegistry],
  requires: [INotebookTracker, IThemeManager],
  activate: async (app: JupyterFrontEnd, notebookTracker:INotebookTracker, themeManager: IThemeManager, settingRegistry: ISettingRegistry | null) => {

    const id_ld: any = window.frameElement.id.split('ld_code_iframe_').pop();

    if (settingRegistry) {
      settingRegistry
        .load(plugin.id)
        .then(settings => {
          console.log('lnb_ro_init: settings loaded [id_labdoc='+id_ld+'].', settings.composite);
        })
        .catch(reason => {
          console.error('lnb_ro_init: failed to load settings [id_labdoc='+id_ld+'].', reason);
        });
    }

    // Activate theme
    notebookTracker.currentChanged.connect(async(tracker, panel) => {
      if (themeManager.theme !== 'lnb_ro_theme') {
         await themeManager.setTheme('lnb_ro_theme');
      }
      console.log("lnb_ro_theme: lnb_ro_theme is activated [id_labdoc="+id_ld+"]");
    });

    // Send a request to host page
    window.parent.postMessage(
        {
            type: 'init_ro_notebook',
            id_ld: id_ld
        },
    '*');
    console.log("lnb_ro_init -> lnb: init_ro_notebook request [id_labdoc="+id_ld+"]");

    let hostResponsePromise = () => new Promise((resolve, reject) => {
		window.addEventListener('message', event => {
			if (event.data.type === 'init_ro_notebook') {
				resolve(event.data);
			}
		}, {once: true});
	});

    // New promise for timeout purpose
	let timeOutPromise = () => new Promise(function(resolve, reject) {
		setTimeout(function() {
			resolve({});
		}, 5000);
	});

    // Wait for host response 
    let response: any = await Promise.race([hostResponsePromise(), timeOutPromise()]);

    notebookTracker.currentChanged.connect(async(tracker, panel) => {
        if (response.hasOwnProperty('ld_content')){
            if (panel != null){

                await panel.revealed;
                await panel.sessionContext.ready;
                panel.content.activeCellIndex = 0;

                // 1 - Initialize content
                const model = new NotebookModel();
                model.fromJSON(JSON.parse(response.ld_content));
                panel.content.model = model;

                // 2 - Hide utils_content cell
                const tag = "utils_functions";
                panel.content.widgets.forEach((cell: Cell<ICellModel>, index: number) => {
                    let arrayOfTags = cell.model.metadata.get("tags");
                    if (Array.isArray(arrayOfTags) && arrayOfTags != null && arrayOfTags != undefined){
                        if (arrayOfTags.includes(tag)){
                            let layout = panel.content.widgets[index].layout as unknown as PanelLayout;
                            for (var i=0; i<5; i++){
                                layout.widgets[i].hide();
                            }
                            if (typeof document.querySelectorAll('div.jp-Cell-inputWrapper[aria-hidden="true"]')[0] !== 'undefined'){
                                (document.querySelectorAll('div.jp-Cell-inputWrapper[aria-hidden="true"]')[0]
                                    .closest('div.jp-Cell') as HTMLElement)
                                    .style.display = 'none';
                            }
                        }
                    }
                });

                console.log("lnb_ro_init -> lnb: init_ro_notebook received [id_labdoc="+id_ld+"]");

                // 3 - Adjust iframe height dynamically
                await panel.revealed;
                await panel.sessionContext.ready;
                panel.content.activeCellIndex = 0;
                const height = document.getElementsByClassName('jp-NotebookPanel-notebook')[0].scrollHeight;
                console.log("lnb_ro_init -> lnb: resize_iframe with "+height+"px [id_labdoc="+id_ld+"]");
                window.parent.postMessage(
                    {
                        type: 'resize_iframe', 
                        height: height,
                        id_ld: id_ld
                    },
                '*');

                // 4 - Freeze cells and disable execution
                panel.content.widgets.forEach((cell: Cell<ICellModel>, index: number) => {
                    cell.model.metadata.set('editable', false) 
                    //cell.readOnly = true;
                    const tag = "info_attached_files_from_lnb";
                    const arrayOfTags = cell.model.metadata.get("tags");
                    if (Array.isArray(arrayOfTags) && arrayOfTags != null && arrayOfTags != undefined){
                        if (arrayOfTags.includes(tag)){
                            model.cells.remove(index);
                        }
                    }
                });

                // 5 - Shut down kernel
                try {
                    panel.sessionContext.shutdown();
                } catch(ex) {
                    console.log("kernel is shut down [id_labdoc="+id_ld+"]");
                }

                console.log("lnb_ro_init: kernel shut down and cells freezed [id_labdoc="+id_ld+"]");


                // 6 - Send message back to lnb
                window.parent.postMessage(
                    {
                        type: 'ld_content_loaded',
                        id_ld: id_ld
                    },
                '*');
                console.log("lnb_ro_init -> lnb: ld_content_loaded [id_labdoc="+id_ld+"]");

            } else {
                console.error("lnb_ro_init -> lnb: error while initializing the notebook [id_labdoc="+id_ld+"]");
            }
        }
    });

  }
};

export default plugin;
